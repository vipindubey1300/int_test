import 'package:flutter/material.dart';
import 'package:int_test/screens/home.dart';
import 'package:int_test/screens/splash.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
        home: Splash(),//MainScreen()
        routes: <String, WidgetBuilder>{
          '/home':  (context) => new Home(),
        }
    );
  }
}

