import 'package:int_test/api/api_repository.dart';
import 'package:int_test/api/api_response.dart';
import 'dart:async';

import 'package:int_test/bloc/bloc.dart';
import 'package:int_test/models/home_data.dart';


class HomeBloc implements Bloc{
  ApiRepository _apiRepository;

  StreamController _homeDataController;

  StreamSink<ApiResponse<Homedata>> get homeDataSink =>
      _homeDataController.sink;

  Stream<ApiResponse<Homedata>> get homeDataStream =>
      _homeDataController.stream;

  HomeBloc() {
    _homeDataController = StreamController<ApiResponse<Homedata>>();
    _apiRepository = ApiRepository();
    fetchData();
  }

  fetchData() async {
    homeDataSink.add(ApiResponse.loading('Fetching Home Data.'));
    try {
      Homedata data = await _apiRepository.getHomeData();
      homeDataSink.add(ApiResponse.completed(data));
    } catch (e) {
      homeDataSink.add(ApiResponse.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _homeDataController?.close();
  }
}