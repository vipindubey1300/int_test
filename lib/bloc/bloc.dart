
//All of your BLoC classes will conform to this interface.
// The interface does not do much except force you to add a dispose method.

abstract class Bloc {
  void dispose();
}