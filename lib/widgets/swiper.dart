import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:int_test/utils/app_colors.dart';



class SwiperView extends StatefulWidget {
  @override
  _SwiperState createState() => new _SwiperState();
}
class _SwiperState extends State<SwiperView> {
  int _current = 0;
  final List temp = [1,2,3,4];


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CarouselSlider(
          options: CarouselOptions(
              height: 160,
             aspectRatio: 16/9,
            viewportFraction: 0.8,
              initialPage: 0,
              enableInfiniteScroll: true,
              reverse: false,
              autoPlay: true,
              autoPlayInterval: Duration(seconds: 3),
              autoPlayAnimationDuration: Duration(milliseconds: 800),
              autoPlayCurve: Curves.fastOutSlowIn,
              enlargeCenterPage: true,
              scrollDirection: Axis.horizontal,
              onPageChanged: (index,reason) {
              setState(() => _current = index);
            },
          ),


          items: temp.map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    decoration: BoxDecoration(
                        color: AppColors.PRIMARY_COLOR,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Image.asset("assets/images/banner.png",),
                );
              },
            );
          }).toList(),


        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: temp.map((e) {
            int index = temp.indexOf(e);
            return Container(
                width: 8.0,
                height: 8.0,
                margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                decoration: BoxDecoration(
                shape: BoxShape.circle,
                 color: _current == index
                ? Color.fromRGBO(0, 0, 0, 0.9)
                    : Color.fromRGBO(0, 0, 0, 0.4)),
                );
        }).toList()
          ),
        ]
        );



  }
}