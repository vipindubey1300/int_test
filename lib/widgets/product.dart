import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:int_test/models/home_data.dart';
import 'package:int_test/utils/app_colors.dart';



class ProductItem extends StatelessWidget {
   ProductItem({Key key, @required this.product, @required this.index}) : super(key: key);
  final Data product;
  final int index;

  @override
  Widget build(BuildContext context) {
    int _containerWidth = 200;
    return Stack(
      children: <Widget>[
        Container(
          clipBehavior: Clip.antiAliasWithSaveLayer,
          margin: EdgeInsets.fromLTRB(20, 6, 20, 6),
          width: _containerWidth.toDouble(),
            decoration: new BoxDecoration(
                //color: Colors.green,
                borderRadius: new BorderRadius.only(
                  bottomLeft: const Radius.circular(20.0),
                  bottomRight: const Radius.circular(20.0),
                )
            ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 8,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: const Radius.circular(20.0),
                    bottomRight: const Radius.circular(20.0),
                  ),
                  child:   Image.asset(index % 2 == 0 ? "assets/images/img1.png" : "assets/images/img2.png",
                    width: _containerWidth.toDouble(),fit: BoxFit.fill,)
                ),
              ),
              Expanded(flex: 2,child:Padding(
                padding: EdgeInsets.fromLTRB(3,12,0,3),
                child:  Text(product.name,style: TextStyle(color: AppColors.PRIMARY_COLOR,fontWeight: FontWeight.bold,fontSize: 16),),
              ),),
              Expanded(flex: 2,child:
              Padding(padding: EdgeInsets.all(3),child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('₹ ${product.price}',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700),),
                  Text('₹ 324',style: TextStyle(decoration: TextDecoration.lineThrough,color: Colors.grey,fontSize: 12),)
                ],
              ),)),
              Expanded(
                flex: 2,
                child: SizedBox(
                  width: double.infinity,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: const Radius.circular(20.0),
                          bottomRight: const Radius.circular(20.0),
                        ),
                        side: BorderSide(color:  AppColors.PRIMARY_COLOR)),
                    onPressed: () {},
                    color: AppColors.PRIMARY_COLOR,
                    textColor: Colors.white,
                    child: Text("Add to cart".toUpperCase(),
                        style: TextStyle(fontSize: 14)),
                  ),
                ),
              )

            ],
          ),
        ),
        Positioned(
          top: 10,right: 25,
          child: Container(
            height: 30,width: 30,
            decoration: BoxDecoration(
              color: Colors.white,borderRadius: BorderRadius.circular(20),
            ),
            child: Center(child: Icon(index % 2 == 0 ? Icons.favorite : Icons.favorite_border,color: Colors.red,),),
          ),
        ),
        Positioned(
          top: 10,left: 25,
          child: Container(
            color: Colors.red,
             padding:EdgeInsets.all(4) ,
            child: Text("15%off",style: TextStyle(color: Colors.white),),
          ),
        )
      ],
    );
  }
}