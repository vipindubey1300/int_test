import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';



class Category extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 6, 20, 6),
      child: Center(
        child: Column(
          children: <Widget>[
            Container(
              height: 70,
              width: 70,
              child: Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Center(child: Image.asset("assets/images/category.png",height: 34,width: 35,),),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
            ),
            ),
            SizedBox(height: 15,),
            Text("Category"),
          ],
        ),
      ),
    );
  }
}