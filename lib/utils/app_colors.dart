import 'package:flutter/material.dart';

// App Colors Class - Resource class for storing app level color constants
class AppColors {
  static const Color PRIMARY_COLOR = Color(0xFF064127);
  static const Color SECONDARY_COLOR = Color(0xFFC2D835);
}