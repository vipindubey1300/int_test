import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:int_test/api/api_exceptions.dart';
import 'package:http/http.dart' as http;

/*
For making communication between our Remote server and Application we use various APIs which needs some type of HTTP methods to get executed.
So we are first going to create a base API helper class, which will be going to help us communicate with our server.
*/



class ApiBaseHelper {
  Future<dynamic> get(String _url) async {
    var responseJson;
    try {
      final response = await http.get(_url);
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
       // print(responseJson);
        return responseJson;
      case 400:
      throw BadRequestException(response.body.toString());
      case 401:
      case 403:
      throw UnauthorisedException(response.body.toString());
      case 500:
      default:
      throw FetchDataException('Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }

}