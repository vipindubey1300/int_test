import 'package:int_test/api/api_base_helper.dart';
import 'package:int_test/models/home_data.dart';
import 'package:int_test/models/home_data.dart';
import 'package:int_test/utils/constants.dart';

/*
We are going to use a Repository class which going to act as the intermediator and a layer of abstraction between the APIs and the BLOC.
 The task of the repository is to deliver movies data to the BLOC after fetching it from the API.
*/


class ApiRepository {

  ApiBaseHelper _helper = ApiBaseHelper();
  final String url = AppConstants.URL;

  Future<Homedata> getHomeData() async {
    final response = await _helper.get(url);
    var res = Homedata.fromJson(response);
    return res ;
  }
}