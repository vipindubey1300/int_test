import 'package:flutter/material.dart';

import 'package:int_test/api/api_response.dart';
import 'package:int_test/bloc/bloc_provider.dart';
import 'package:int_test/bloc/home_bloc.dart';
import 'package:int_test/models/home_data.dart';
import 'package:int_test/utils/app_colors.dart';
import 'package:int_test/widgets/category.dart';
import 'package:int_test/widgets/product.dart';
import 'package:int_test/widgets/swiper.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home>{
  int _index = 3;
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey();


  @override
  void initState() {
    super.initState();
  }
  final bloc = HomeBloc();
  @override
  Widget build(BuildContext context) {
    return BlocProvider<HomeBloc>(
      bloc: bloc,
      child: new Scaffold(
         resizeToAvoidBottomInset: false,
          key: _scaffoldKey,
          body: new SafeArea(
              child:_buildResults(bloc),
          )
      ),
    );
  }

  Widget _buildResults(HomeBloc bloc) {
    return StreamBuilder<ApiResponse<Homedata>>(
      stream: bloc.homeDataStream,
      initialData: null,
      builder: (context, snapshot) {
        final results = snapshot.data;
        if((results != null && results.data == null)) return Align(alignment: Alignment.center,child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            CircularProgressIndicator(),
            SizedBox(height: 20,),
            Text("Please Wait ",style: TextStyle(color: AppColors.PRIMARY_COLOR),)
          ],
        ),);
        if (results == null) return Center(child: Text('No Data Found'));
        else return _buildView(snapshot.data.data.data);
      },
    );
  }

  Widget _appBar(){
    return(
      Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            children: <Widget>[
             IconButton(
               icon:  Icon(Icons.menu,color: Colors.white,),
               onPressed: () {
                 print("your menu action here");
                 //will used later in future to open drawer
                 _scaffoldKey.currentState.openDrawer();
               },
             ),
              SizedBox(width: 10,),
              Image.asset("assets/images/logo.png",height: 40,width: 100,),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(Icons.notifications,color: Colors.white,),
              SizedBox(width: 10,),
              Icon(Icons.shopping_cart,color: Colors.white,),

            ],
          )
        ],
      )
    );
  }

  Widget _buildView(List _data){
    return Column(
        children: <Widget>[
          Container(
            height: 60,
            color: AppColors.PRIMARY_COLOR,
            child: _appBar(),
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          ),
            Expanded(
              child: SingleChildScrollView(
                  child:Column(
                    children: <Widget>[
                      //category container
                      Stack(
                        children: <Widget>[
                          Container(
                            color: AppColors.PRIMARY_COLOR,
                            height: 200.0,
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 40),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                TextField(
                                  decoration: InputDecoration(
                                    fillColor: Colors.white,
                                    contentPadding: EdgeInsets.all(10),
                                    border: new OutlineInputBorder(
                                      borderRadius: const BorderRadius.all(
                                        const Radius.circular(10.0),
                                      ),
                                    ),
                                    filled: true,
                                    hintText: 'What are you looking for today ?',
                                    suffixIcon: new GestureDetector(
                                      onTap: () {},
                                      child: new Container(
                                        decoration: BoxDecoration(
                                            color: AppColors.SECONDARY_COLOR,
                                            borderRadius:new BorderRadius.only(
                                              topRight: const Radius.circular(10.0),
                                              bottomRight: const Radius.circular(10.0),
                                            )),

                                        child: Icon(Icons.search),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Category",style: TextStyle(color:Colors.white,fontSize: 17,fontWeight: FontWeight.w700),),
                                    RaisedButton(
                                      shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(18.0),
                                          side: BorderSide(color: AppColors.PRIMARY_COLOR)),
                                      onPressed: () {},
                                      color: Colors.white,
                                      textColor: AppColors.PRIMARY_COLOR,
                                      child: Text("View All",
                                          style: TextStyle(fontSize: 14)),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 10.0,right: 10.0, top:160.0),
                            child: Container(
                              color: Colors.transparent,
                              height: 120.0,
                              width: MediaQuery.of(context).size.width,
                              child: ListView.builder(
                                  shrinkWrap: true,
                                  //primary: false,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 8,
                                  itemBuilder: (context, index){
                                    return Category();
                                  }),
                            ),
                          )
                        ],
                      ),
                      Container(
                        height: 200,
                        child: SwiperView(),
                      ),
                      Container(
                        margin: EdgeInsets.all(10),
                        child: Column(
                          children: <Widget>[
                            SizedBox(height:20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.fromLTRB(10,10,20,10),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 2.0,color: AppColors.PRIMARY_COLOR
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                      'New Arrivals',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700)
                                  ),
                                ),
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: AppColors.SECONDARY_COLOR)),
                                  onPressed: () {},
                                  color: AppColors.SECONDARY_COLOR,
                                  textColor: Colors.black45,
                                  child: Text("View all",
                                      style: TextStyle(fontSize: 14)),
                                )
                              ],
                            ),
                            SizedBox(height:13),
                            Container(
                              height: 260,
                              child:  ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: _data.length,
                                  itemBuilder: (context, index){
                                    return ProductItem(product: _data[index],index: index,);
                                  }),
                            ),
                            SizedBox(height:20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.fromLTRB(10,10,20,10),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 2.0,color: AppColors.PRIMARY_COLOR
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                      'Trending Now',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700)
                                  ),
                                ),
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: AppColors.SECONDARY_COLOR)),
                                  onPressed: () {},
                                  color: AppColors.SECONDARY_COLOR,
                                  textColor: Colors.black45,
                                  child: Text("View all",
                                      style: TextStyle(fontSize: 14)),
                                )
                              ],
                            ),
                            SizedBox(height:13),
                            Container(
                              height: 260,
                              child:  ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 4,
                                  itemBuilder: (context, index){
                                    return ProductItem(product: _data[index],index: index,);
                                  }),
                            ),
                            SizedBox(height:20),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.fromLTRB(10,10,20,10),
                                  decoration: BoxDecoration(
                                    border: Border(
                                      bottom: BorderSide(
                                          width: 2.0,color: AppColors.PRIMARY_COLOR
                                      ),
                                    ),
                                  ),
                                  child: Text(
                                      'All Time Favourites',style: TextStyle(fontSize: 16,fontWeight: FontWeight.w700)
                                  ),
                                ),
                                RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(color: AppColors.SECONDARY_COLOR)),
                                  onPressed: () {},
                                  color: AppColors.SECONDARY_COLOR,
                                  textColor: Colors.black45,
                                  child: Text("View all",
                                      style: TextStyle(fontSize: 14)),
                                )
                              ],
                            ),
                            SizedBox(height:13),
                            Container(
                              height: 260,
                              child:  ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  scrollDirection: Axis.horizontal,
                                  itemCount: 4,
                                  itemBuilder: (context, index){
                                    return ProductItem(product: _data[index],index: index,);
                                  }),
                            )
                          ],
                        ),
                      )
                    ],
                  )
              ),
            )
        ],

    );
  }
}
