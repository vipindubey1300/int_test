import 'package:flutter/material.dart';
import 'dart:async';

import 'package:int_test/utils/app_colors.dart';


class Splash extends StatefulWidget {

  @override
  _SplashState createState() => new _SplashState();
}

class _SplashState extends State<Splash>{

  startTime() async {
    var _duration = new Duration(seconds: 1);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() async{
     Navigator.pushReplacementNamed(context, '/home');
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Container(
          color: AppColors.PRIMARY_COLOR,
          child: Center(
            child: Image.asset("assets/images/logo.png",height: 150,width: 150,),
          ),
        )
    );
  }
}
